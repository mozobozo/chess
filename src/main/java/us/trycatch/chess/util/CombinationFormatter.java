package us.trycatch.chess.util;

import us.trycatch.chess.model.pieces.ChessPiece;
import us.trycatch.chess.model.Combination;

/**
 * Creates a String representation of Combination.
 */
public final class CombinationFormatter {

    public static final String PIECE_FORMAT = "%-6s";
    public static final String DIVIDER = "|";

    public static String format(Combination combination) {
        StringBuilder result = new StringBuilder();

        ChessPiece[][] board = combination.getBoard();

        for (int row = 0; row < board.length; row++) {
            result.append(DIVIDER);
            for (int col = 0; col < board[0].length; col++) {
                ChessPiece piece = board[row][col];
                result.append(formatChessPiece(piece)).append(DIVIDER);
            }
            result.append("\n");
        }

        return result.toString();
    }

    private static String formatChessPiece(ChessPiece piece) {
        return String.format(PIECE_FORMAT, piece != null ? piece.describe() : "");
    }
}
