package us.trycatch.chess.util;

import us.trycatch.chess.model.Cell;
import us.trycatch.chess.model.pieces.ChessPiece;
import us.trycatch.chess.model.Combination;

import static us.trycatch.chess.model.Cell.newCell;

/**
 * Used to create Combinations.
 */
public class CombinationBuilder {

    private Combination combination;

    public CombinationBuilder(int boardWidth, int boardHeight) {
        combination = new Combination(boardWidth, boardHeight);
    }

    public CombinationBuilder add(ChessPiece piece, int n) {
        for (int i = 0; i < n; i++) {
            combination.addPiece(piece);
        }
        return this;
    }

    public CombinationBuilder place(ChessPiece piece, int x, int y) {
        combination.getBoard()[x][y] = piece;

        Cell occupiedCell = newCell(x, y);
        combination.getOccupiedCells().add(occupiedCell);
        combination.getAvailableCells().remove(occupiedCell);
        return this;
    }

    public Combination build() {
        return combination;
    }
}
