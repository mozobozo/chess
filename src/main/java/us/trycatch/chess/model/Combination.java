package us.trycatch.chess.model;

import us.trycatch.chess.model.pieces.ChessPiece;
import us.trycatch.chess.util.CombinationFormatter;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import static us.trycatch.chess.model.Cell.newCell;

/**
 * Combination describes state of the board, occupied and available cells lists, and available pieces.
 * Cell is <b>available</b> if it is free and not threatened by any other piece.
 */
public class Combination {

    private ChessPiece[][] board;

    private List<Cell> occupiedCells = new LinkedList<>();
    private List<ChessPiece> chessPieces = new LinkedList<>();

    private HashSet<Cell> availableCells = new HashSet<>();

    public Combination(int boardWidth, int boardHeight) {
        board = new ChessPiece[boardWidth][boardHeight];

        for (int w = 0; w < boardWidth; w ++) {
            for (int h = 0; h < boardHeight; h ++) {
                availableCells.add(newCell(w, h));
            }
        }
    }

    private Combination(Combination src) {
        this.board = new ChessPiece[src.board.length][];
        for (int i = 0; i < this.board.length; i++) {
            this.board[i] = src.board[i].clone();
        }

        this.occupiedCells.addAll(src.occupiedCells);
        this.chessPieces.addAll(src.chessPieces);
        this.availableCells.addAll(src.availableCells);
    }

    public boolean hasMorePieces() {
        return !chessPieces.isEmpty();
    }

    /**
     * Checks if this piece will threaten any already occupied cell from a given cell.
     */
    public boolean canPlace(ChessPiece piece, Cell cell) {
        for (Cell occupiedCell : occupiedCells) {
            if (piece.isThreatening(cell.getX(), cell.getY(), occupiedCell.getX(), occupiedCell.getY())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Places a piece on a cell. All threatened cells are removed from available.
     * @return Original combination is not changed. Modified copy is returned.
     * */
    public Combination place(ChessPiece piece, Cell cell) {
        Combination copy = new Combination(this);
        copy.availableCells.remove(cell);
        copy.occupiedCells.add(cell);

        copy.chessPieces.remove(piece);
        copy.board[cell.getX()][cell.getY()] = piece;

        HashSet<Cell> notThreatenedCells = new HashSet<>();
        for (Cell availableCell : copy.availableCells) {
            if (!piece.isThreatening(cell.getX(), cell.getY(), availableCell.getX(), availableCell.getY())) {
                 notThreatenedCells.add(availableCell);
            }
        }
        copy.availableCells = notThreatenedCells;

        return copy;
    }

    /** Adds a piece to a list of available pieces. */
    public void addPiece(ChessPiece chessPiece) {
        chessPieces.add(chessPiece);
    }

    public ChessPiece[][] getBoard() {
        return board;
    }

    public Collection<Cell> getAvailableCells() {
        return availableCells;
    }

    public List<ChessPiece> getChessPieces() {
        return chessPieces;
    }

    public Collection<Cell> getOccupiedCells() {
        return occupiedCells;
    }

    @Override
    public String toString() {
        return CombinationFormatter.format(this);
    }
}
