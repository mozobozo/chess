package us.trycatch.chess.model;

import java.util.HashMap;

/**
 * Represents a board cell.
 * Cells are used a lot so instead of creating them each time they are cached and reused.
 */
public class Cell {
    private int x;
    private int y;

    private int hashCode;
    private static HashMap<Integer, Cell> cellsCache = new HashMap<>();

    private Cell(int x, int y) {
        this.x = x;
        this.y = y;
        this.hashCode = hash(x, y);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cell cell = (Cell) o;

        if (x != cell.x) return false;
        if (y != cell.y) return false;

        return true;
    }

    public static Cell newCell(int x, int y) {
        int cellCacheKey = hash(x, y);
        if (!cellsCache.containsKey(cellCacheKey)) {
            cellsCache.put(cellCacheKey, new Cell(x, y));
        }
        return cellsCache.get(cellCacheKey);
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    private static int hash(int x, int y) {
        return 31 * x + y;
    }

    @Override
    public String toString() {
        return String.format("[%s,%s]", x, y);
    }
}
