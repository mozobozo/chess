package us.trycatch.chess.model.pieces;

import static java.lang.Math.abs;

public class Queen implements ChessPiece {

    Queen() {}

    @Override
    public boolean isThreatening(int fromX, int fromY, int x, int y) {

        boolean sameLine = fromX == x || fromY == y;
        boolean sameDiagonal = abs(fromX - x) == abs(fromY - y);

        return sameLine || sameDiagonal;
    }

    @Override
    public String describe() {
        return "Queen";
    }
}
