package us.trycatch.chess.model.pieces;

public class Knight implements ChessPiece {

    Knight() {}

    @Override
    public boolean isThreatening(int fromX, int fromY, int x, int y) {
        int distX = Math.abs(fromX - x);
        int distY = Math.abs(fromY - y);
        return (distX == 2 && distY == 1) || (distX == 1 && distY == 2);
    }

    @Override
    public String describe() {
        return "Knight";
    }
}
