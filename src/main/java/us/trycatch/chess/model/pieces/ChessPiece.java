package us.trycatch.chess.model.pieces;

public interface ChessPiece {

    Queen QUEEN = new Queen();
    Rook ROOK = new Rook();
    Bishop BISHOP = new Bishop();
    Knight KNIGHT = new Knight();
    King KING = new King();


    boolean isThreatening(int fromX, int fromY, int x, int y);

    String describe();
}
