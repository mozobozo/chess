package us.trycatch.chess.model.pieces;

public class Bishop implements ChessPiece {

    Bishop() {}

    @Override
    public boolean isThreatening(int fromX, int fromY, int x, int y) {
        return Math.abs(fromX - x) == Math.abs(fromY - y);
    }

    @Override
    public String describe() {
        return "Bishop";
    }
}
