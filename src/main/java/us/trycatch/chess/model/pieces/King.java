package us.trycatch.chess.model.pieces;

import static java.lang.Math.abs;

public class King implements ChessPiece {

    King() {}

    @Override
    public boolean isThreatening(int fromX, int fromY, int x, int y) {
        return abs(fromX - x) <= 1 && abs(fromY - y) <= 1;
    }

    @Override
    public String describe() {
        return "King";
    }
}
