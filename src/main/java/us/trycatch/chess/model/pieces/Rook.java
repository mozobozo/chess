package us.trycatch.chess.model.pieces;

public class Rook implements ChessPiece {

    Rook() {}

    @Override
    public boolean isThreatening(int fromX, int fromY, int x, int y) {
        return fromX == x || fromY == y;
    }

    @Override
    public String describe() {
        return "Rook";
    }
}
