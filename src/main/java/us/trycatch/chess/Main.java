package us.trycatch.chess;

import us.trycatch.chess.combinator.Combinator;
import us.trycatch.chess.combinator.listeners.CombinationsPrinter;
import us.trycatch.chess.model.Combination;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner inputScanner = new Scanner(System.in);

        System.out.println("Enter size of the board in format MxN. i.e. '3x2':");
        String input = inputScanner.nextLine() + "\n";

        System.out.println("Enter chess pieces counts in format 'Q R B N K'. i.e. '1 0 2':");
        input +=inputScanner.nextLine();

        Combination initialCombination = InputParser.parse(input);

        CombinationsPrinter combinationListener = new CombinationsPrinter();
        Combinator.findSafeCombinations(initialCombination, combinationListener);
    }
}
