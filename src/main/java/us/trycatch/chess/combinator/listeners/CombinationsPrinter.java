package us.trycatch.chess.combinator.listeners;

import us.trycatch.chess.model.Combination;
import us.trycatch.chess.util.CombinationFormatter;

/**
 * Prints found combinations to output.
 */
public class CombinationsPrinter extends CombinationCounter {

    @Override
    public void combinationFound(Combination combination) {
        super.combinationFound(combination);
        System.out.println(CombinationFormatter.format(combination));
    }
}
