package us.trycatch.chess.combinator;

import us.trycatch.chess.model.Cell;
import us.trycatch.chess.model.Combination;
import us.trycatch.chess.model.pieces.ChessPiece;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class RecursionKey {

    private Map<Integer, Class> usedPieces = new HashMap<>();
    private int hashCode;

    public RecursionKey(Combination combination) {
        Collection<Cell> occupiedCells = combination.getOccupiedCells();
        ChessPiece[][] board = combination.getBoard();
        for (Cell occupiedCell : occupiedCells) {
            usedPieces.put(occupiedCell.hashCode(), board[occupiedCell.getX()][occupiedCell.getY()].getClass());
        }
        Object[] hashes = usedPieces.keySet().toArray();
        Arrays.sort(hashes);
        hashCode = Arrays.hashCode(hashes);
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public boolean equals(Object o) {
        RecursionKey that = (RecursionKey) o;

        if (usedPieces.size() != that.usedPieces.size()) {
            return false;
        }

        for (Integer cellHash : usedPieces.keySet()) {
            if (usedPieces.get(cellHash) != that.usedPieces.get(cellHash)) {
                return false;
            }
        }

        return true;
    }
}