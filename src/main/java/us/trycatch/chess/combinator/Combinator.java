package us.trycatch.chess.combinator;

import us.trycatch.chess.model.Cell;
import us.trycatch.chess.model.Combination;
import us.trycatch.chess.model.pieces.ChessPiece;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicLong;

import static java.util.concurrent.TimeUnit.SECONDS;

public class Combinator {

    private final Map<RecursionKey, Boolean> keys = new ConcurrentHashMap<>();

    private final ThreadPoolExecutor executor;
    private final CombinationListener combinationListener;

    private final AtomicLong activeTasksCount = new AtomicLong(0);

    private Combinator(CombinationListener combinationListener) {
        int processorsNumber = Runtime.getRuntime().availableProcessors();
        this.executor = new ThreadPoolExecutor(processorsNumber, processorsNumber * 2, 10L, SECONDS, new LinkedBlockingQueue<Runnable>());

        this.combinationListener = combinationListener;
    }

    /**
     * Recursively looks for all unique configurations for which all of
     * the pieces are placed on the board without threatening each other.
     *
     * @param combinationListener - will be receiving notifications on each found combination.
     */
    public static void findSafeCombinations(Combination initialCombination, CombinationListener combinationListener) {
        Combinator combinator = new Combinator(combinationListener);
        combinator.fork(initialCombination);
        combinator.waitForWorkToComplete();
    }

    private void waitForWorkToComplete() {
        do {
            synchronized (this) {
                try {
                    // notified from MakeMovesTask below
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } while (executor.getQueue().size() > 0 || activeTasksCount.get() > 0);

        executor.shutdown();
        combinationListener.processFinished();
    }

    /**
     * Recursive step that first checks if combination is what we are looking for or invalid.
     * In first case it notifies combinationListener about it or just returns in case of invalid (no pieces or available cells left).
     * If none combination is neither invalid nor valid - it takes next available piece and recursively tries to place it on any available cell.
     *
     * Recursive calls may or may not be forked to a separate thread.
     */
    private void doPossibleMoves(Combination combination) {
        if (!combination.hasMorePieces()) {
            // we found a valid combination
            combinationListener.combinationFound(combination);
            return;
        }

        int piecesAvailable = combination.getChessPieces().size();
        int availableCells = combination.getAvailableCells().size();
        if (availableCells == 0 || availableCells < piecesAvailable) {
            // combination is invalid. stop recursion
            return;
        }

        // take next piece to try
        ChessPiece piece = combination.getChessPieces().get(0);
        // and apply for each still available cell
        for (Cell cell : combination.getAvailableCells()) {
            if (combination.canPlace(piece, cell)) {

                // get copy of combination with current piece on current cell
                Combination newCombination = combination.place(piece, cell);

                // check if we already had similar combinations
                if (offerRecursion(new RecursionKey(newCombination))) {
                    if (shouldFork(combination)) {
                        fork(newCombination);
                    }
                    else {
                        doPossibleMoves(newCombination);
                    }
                }
            }
        }
    }

    private boolean offerRecursion(RecursionKey key) {
        return keys.put(key, Boolean.TRUE) == null;
    }

    private boolean shouldFork(Combination combination) {
        return combination.getChessPieces().size() > 2;
    }

    /**
     * Submits this combination to be processed in a separate thread.
     */
    private void fork(Combination combination) {
        executor.execute(new MakeMovesTask(combination));
    }

    /**
     * Runnable wrapper for recursive calls.
     */
    private class MakeMovesTask implements Runnable {

        private Combination combination;

        private MakeMovesTask(Combination combination) {
            this.combination = combination;
        }

        @Override
        public void run() {
            activeTasksCount.getAndIncrement();

            doPossibleMoves(combination);

            // notify if no more active tasks running
            // wait() was called in waitForWorkToComplete().
            if (activeTasksCount.decrementAndGet() == 0) {
                synchronized (Combinator.this) {
                    Combinator.this.notify();
                }
            }
        }
    }
}
