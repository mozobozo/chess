package us.trycatch.chess.combinator;

import us.trycatch.chess.model.Combination;

/**
 * Will be called for each found valid combination.
 */
public interface CombinationListener {

    void combinationFound(Combination combination);

    void processFinished();
}
