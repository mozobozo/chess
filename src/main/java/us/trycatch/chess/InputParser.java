package us.trycatch.chess;

import us.trycatch.chess.model.Combination;
import us.trycatch.chess.util.CombinationBuilder;

import java.util.Scanner;

import static us.trycatch.chess.model.pieces.ChessPiece.*;

/**
 * Parses string input into a Combination.
 * Accepts strings in format "MxN \n Q R B N K". All piece count numbers are optional.
 * For example "4x3 \n 1 0 2" means board 4x3 with one queen and two bishops.
 **/
public final class InputParser {

    private InputParser() { }

    public static Combination parse(String input) {
        Scanner scanner = new Scanner(input);

        String[] size = scanner.nextLine().trim().split("x");

        int width = Integer.valueOf(size[0]);
        int height = Integer.valueOf(size[1]);

        CombinationBuilder combinationBuilder = new CombinationBuilder(width, height);
        combinationBuilder.add(QUEEN, optionalInt(scanner));
        combinationBuilder.add(ROOK, optionalInt(scanner));
        combinationBuilder.add(BISHOP, optionalInt(scanner));
        combinationBuilder.add(KNIGHT, optionalInt(scanner));
        combinationBuilder.add(KING, optionalInt(scanner));

        return combinationBuilder.build();
    }

    private static int optionalInt(Scanner scanner) {
        if (scanner.hasNextInt()) {
            return scanner.nextInt();
        }
        return 0;
    }
}
