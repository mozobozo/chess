package us.trycatch.chess.model.pieces;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static us.trycatch.chess.model.pieces.ChessPiece.*;

public class ChessPieceTest {

    @Test
    public void testQueen() {
        // diagonals
        assertThat(QUEEN.isThreatening(2, 2, 1, 1), is(true));
        assertThat(QUEEN.isThreatening(2, 2, 3, 3), is(true));
        assertThat(QUEEN.isThreatening(2, 2, 3, 1), is(true));
        assertThat(QUEEN.isThreatening(2, 2, 1, 3), is(true));
        assertThat(QUEEN.isThreatening(2, 2, 4, 4), is(true));

        // sides
        assertThat(QUEEN.isThreatening(2, 2, 3, 2), is(true));
        assertThat(QUEEN.isThreatening(2, 2, 2, 3), is(true));
        assertThat(QUEEN.isThreatening(2, 2, 1, 2), is(true));
        assertThat(QUEEN.isThreatening(2, 2, 2, 1), is(true));
    }

    @Test
    public void testBishop() {
        assertThat(BISHOP.isThreatening(2, 2, 1, 1), is(true));
        assertThat(BISHOP.isThreatening(2, 2, 3, 3), is(true));
        assertThat(BISHOP.isThreatening(2, 2, 3, 1), is(true));
        assertThat(BISHOP.isThreatening(2, 2, 1, 3), is(true));

        assertThat(BISHOP.isThreatening(2, 2, 3, 2), is(false));
    }

    @Test
    public void testRook() {
        assertThat(ROOK.isThreatening(2, 2, 3, 2), is(true));
        assertThat(ROOK.isThreatening(2, 2, 2, 3), is(true));
        assertThat(ROOK.isThreatening(2, 2, 1, 2), is(true));
        assertThat(ROOK.isThreatening(2, 2, 2, 1), is(true));

        assertThat(ROOK.isThreatening(2, 2, 1, 1), is(false));
    }

    @Test
    public void testKnight() {
        assertThat(KNIGHT.isThreatening(4, 4, 6, 5), is(true));
        assertThat(KNIGHT.isThreatening(4, 4, 5, 6), is(true));

        assertThat(KNIGHT.isThreatening(4, 4, 6, 3), is(true));
        assertThat(KNIGHT.isThreatening(4, 4, 3, 6), is(true));

        assertThat(KNIGHT.isThreatening(4, 4, 3, 2), is(true));
        assertThat(KNIGHT.isThreatening(4, 4, 2, 3), is(true));

        assertThat(KNIGHT.isThreatening(4, 4, 5, 2), is(true));
        assertThat(KNIGHT.isThreatening(4, 4, 2, 5), is(true));
    }

    @Test
    public void testKing() {
        // diagonals
        assertThat(KING.isThreatening(2, 2, 1, 1), is(true));
        assertThat(KING.isThreatening(2, 2, 3, 3), is(true));
        assertThat(KING.isThreatening(2, 2, 3, 1), is(true));
        assertThat(KING.isThreatening(2, 2, 1, 3), is(true));

        // difference from queen
        assertThat(KING.isThreatening(2, 2, 4, 4), is(false));

        // sides
        assertThat(KING.isThreatening(2, 2, 3, 2), is(true));
        assertThat(KING.isThreatening(2, 2, 2, 3), is(true));
        assertThat(KING.isThreatening(2, 2, 1, 2), is(true));
        assertThat(KING.isThreatening(2, 2, 2, 1), is(true));
    }
}
