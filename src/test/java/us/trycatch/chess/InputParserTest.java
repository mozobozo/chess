package us.trycatch.chess;

import org.junit.Test;
import us.trycatch.chess.model.Combination;
import us.trycatch.chess.model.pieces.ChessPiece;

import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static us.trycatch.chess.model.pieces.ChessPiece.*;

public class InputParserTest {

    @Test
    public void testBoardParsing() {
        // MxN \n Q R B N K
        Combination board = InputParser.parse("5x6 \n 1 2 3 4 5");

        assertThat(board.getBoard().length, is(5));
        assertThat(board.getBoard()[0].length, is(6));

        assertThat(countPieces(board.getChessPieces(), QUEEN),  is(1));
        assertThat(countPieces(board.getChessPieces(), ROOK),   is(2));
        assertThat(countPieces(board.getChessPieces(), BISHOP), is(3));
        assertThat(countPieces(board.getChessPieces(), KNIGHT), is(4));
        assertThat(countPieces(board.getChessPieces(), KING),   is(5));

    }

    @Test
    public void testOptionalFieldsParsing() {
        Combination board = InputParser.parse("1x2 \n 1 0 3");

        assertThat(countPieces(board.getChessPieces(), QUEEN),  is(1));
        assertThat(countPieces(board.getChessPieces(), ROOK),   is(0));
        assertThat(countPieces(board.getChessPieces(), BISHOP), is(3));
        assertThat(countPieces(board.getChessPieces(), KNIGHT), is(0));
        assertThat(countPieces(board.getChessPieces(), KING),   is(0));
    }

    private int countPieces(Collection<ChessPiece> pieces, ChessPiece pieceToFind) {
        int count = 0;

        for (ChessPiece piece : pieces) {
            if (piece == pieceToFind) {
                count ++;
            }
        }

        return count;
    }
}
