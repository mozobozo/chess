package us.trycatch.chess.combinator;

import org.junit.Ignore;
import org.junit.Test;
import us.trycatch.chess.combinator.listeneres.CombinationsValidator;
import us.trycatch.chess.combinator.listeners.CombinationCounter;
import us.trycatch.chess.model.Combination;
import us.trycatch.chess.util.CombinationBuilder;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static us.trycatch.chess.model.pieces.ChessPiece.*;

public class CombinatorTest {

    @Test
    public void testEmptyBoard() {
        Combination combination = new CombinationBuilder(4, 4).build();

        CombinationCounter combinationCounter = new CombinationCounter();
        Combinator.findSafeCombinations(combination, combinationCounter);

        assertThat(combinationCounter.getCount(), is(1L));
    }

    @Test
    public void testZeroSizeBoard() {
        Combination combination = new CombinationBuilder(0, 0).add(KING, 100).build();

        CombinationCounter combinationCounter = new CombinationCounter();
        Combinator.findSafeCombinations(combination, combinationCounter);

        assertThat(combinationCounter.getCount(), is(0L));
    }

    @Test
    public void testSimpleCase() {
        Combination combination = new CombinationBuilder(3, 3).add(KING, 4).build();

        CombinationCounter combinationCounter = new CombinationCounter();
        Combinator.findSafeCombinations(combination, combinationCounter);

        assertThat(combinationCounter.getCount(), is(1L));
    }

    @Test
    public void firstExampleTest() {
        Combination combination = new CombinationBuilder(3, 3).add(KING, 2).add(ROOK, 1).build();

        CombinationsValidator combinationsValidator = new CombinationsValidator("first_example_from_task.txt");
        Combinator.findSafeCombinations(combination, combinationsValidator);

        assertThat(combinationsValidator.getNotFoundCombinations().size(), is(0));
        assertThat(combinationsValidator.getUnexpectedCombinations().size(), is(0));
    }

    @Test
    public void secondExampleTest() {
        Combination combination = new CombinationBuilder(4, 4).add(KNIGHT, 4).add(ROOK, 2).build();

        CombinationsValidator combinationsValidator = new CombinationsValidator("second_example_from_task.txt");
        Combinator.findSafeCombinations(combination, combinationsValidator);

        assertThat(combinationsValidator.getNotFoundCombinations().size(), is(0));
        assertThat(combinationsValidator.getUnexpectedCombinations().size(), is(0));
    }

    @Test
    @Ignore("Not really a test - I can't prove that assert is correct :). Make sure to give it a lot of memory (-Xmx4g is good)")
    public void mainTaskTest() {
        Combination combination = new CombinationBuilder(7, 7)
                .add(QUEEN, 2)
                .add(BISHOP, 2)
                .add(KNIGHT, 1)
                .add(KING, 2)
                .build();

        CombinationCounter combinationCounter = new CombinationCounter();
        Combinator.findSafeCombinations(combination, combinationCounter);

        assertThat(combinationCounter.getCount(), is(3063828L));
    }
}
