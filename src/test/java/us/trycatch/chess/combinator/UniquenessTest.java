package us.trycatch.chess.combinator;

import org.junit.Test;
import us.trycatch.chess.combinator.listeneres.UniquenessValidator;
import us.trycatch.chess.model.Combination;
import us.trycatch.chess.util.CombinationBuilder;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static us.trycatch.chess.model.pieces.ChessPiece.KING;
import static us.trycatch.chess.model.pieces.ChessPiece.KNIGHT;
import static us.trycatch.chess.model.pieces.ChessPiece.QUEEN;

/**
 * Tests that no duplicate combinations will be found.
 */
public class UniquenessTest {

    @Test
    public void test5x5Uniqueness() {
        Combination board = new CombinationBuilder(5, 5).add(KING, 5).build();

        UniquenessValidator validator = new UniquenessValidator();
        Combinator.findSafeCombinations(board, validator);

        assertThat(validator.getCount(), is(1974L));
        assertThat(validator.getDuplicates().isEmpty(), is(true));
    }

    @Test
    public void testManyPiecesUniqueness() {
        Combination board = new CombinationBuilder(5, 5).add(KING, 1).add(KNIGHT, 2).add(QUEEN, 1).build();

        UniquenessValidator validator = new UniquenessValidator();
        Combinator.findSafeCombinations(board, validator);

        assertThat(validator.getDuplicates().isEmpty(), is(true));
    }
}
