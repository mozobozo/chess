package us.trycatch.chess.combinator.listeneres;

import us.trycatch.chess.combinator.listeners.CombinationCounter;
import us.trycatch.chess.model.Combination;
import us.trycatch.chess.model.pieces.ChessPiece;

import java.util.LinkedList;
import java.util.List;

/**
 * Stores all previously found combinations and test each new against them.
 * Duplicates are available via getDuplicates() method.
 */
public class UniquenessValidator extends CombinationCounter {

    private List<Combination> alreadyFound = new LinkedList<>();
    private List<Combination> duplicates = new LinkedList<>();

    @Override
    public synchronized void combinationFound(Combination found) {
        super.combinationFound(found);
        for (Combination existing : alreadyFound) {
            if (equal(existing, found)) {
                duplicates.add(found);
                return;
            }
        }

        alreadyFound.add(found);
    }

    private boolean equal(Combination expected, Combination found) {
        ChessPiece[][] expectedBoard = expected.getBoard();
        ChessPiece[][] foundBoard = found.getBoard();

        for (int i = 0; i < expectedBoard.length; i ++) {
            for (int j = 0; j < expectedBoard[0].length; j ++) {
                if (expectedBoard[i][j] != foundBoard[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public List<Combination> getDuplicates() {
        return duplicates;
    }

    @Override
    public long getCount() {
        return alreadyFound.size();
    }
}
