package us.trycatch.chess.combinator.listeneres;

import us.trycatch.chess.combinator.listeners.CombinationsPrinter;
import us.trycatch.chess.model.Combination;
import us.trycatch.chess.model.pieces.ChessPiece;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import static us.trycatch.SolutionLoader.loadFrom;

/**
 * For any matching combination tries to match the expected one.
 * All mismatches are accessible via getNotFoundCombinations() and getUnexpectedCombinations() methods.
 */
public class CombinationsValidator extends CombinationsPrinter {

    private Collection<Combination> expectedCombinations;
    private Collection<Combination> unexpectedCombinations;

    public CombinationsValidator(String filename) {
        expectedCombinations = loadFrom(filename);
        unexpectedCombinations = new LinkedList<>();
    }

    @Override
    public synchronized void combinationFound(Combination found) {
        Iterator<Combination> iterator = expectedCombinations.iterator();

        while (iterator.hasNext()) {
            Combination expected = iterator.next();
            if (equal(expected, found)) {
                iterator.remove();
                return;
            }
        }

        unexpectedCombinations.add(found);
    }

    private boolean equal(Combination expected, Combination found) {
        ChessPiece[][] expectedBoard = expected.getBoard();
        ChessPiece[][] foundBoard = found.getBoard();

        for (int i = 0; i < expectedBoard.length; i ++) {
            for (int j = 0; j < expectedBoard[0].length; j ++) {
                if (expectedBoard[i][j] != foundBoard[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public Collection<Combination> getNotFoundCombinations() {
        return expectedCombinations;
    }

    public Collection<Combination> getUnexpectedCombinations() {
        return unexpectedCombinations;
    }
}
