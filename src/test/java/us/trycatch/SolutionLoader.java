package us.trycatch;

import us.trycatch.chess.model.Combination;
import us.trycatch.chess.model.pieces.ChessPiece;
import us.trycatch.chess.util.CombinationBuilder;
import us.trycatch.chess.util.CombinationFormatter;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * Loads combinations stored in text files. Used for testing.
 * As an example see first_example_from_task.txt
 */
public class SolutionLoader {

    public static Collection<Combination> loadFrom(String filename) {
        Collection<Combination> result = new LinkedList<>();

        String content = readFile(filename);
        String[] combinations = content.split("\n\n");

        for (String combination : combinations) {
            result.add(parseCombination(combination));
        }

        return result;
    }

    private static Combination parseCombination(String input) {
        String[] rows = input.trim().split("\n");

        CombinationBuilder combinationBuilder = new CombinationBuilder(rows[0].trim().length(), rows.length);

        for (int y = 0; y < rows.length; y ++) {
            for (int x = 0; x < rows[0].length(); x ++ ) {
                String row = rows[y].trim();

                ChessPiece piece = parseChessPiece(row.charAt(x));
                combinationBuilder.place(piece, x, y);
            }
        }

        System.out.println(CombinationFormatter.format(combinationBuilder.build()));

        return combinationBuilder.build();
    }

    private static ChessPiece parseChessPiece(char pieceCode) {
        switch (pieceCode) {
            case 'Q':
                return ChessPiece.QUEEN;
            case 'K':
                return ChessPiece.KING;
            case 'N':
                return ChessPiece.KNIGHT;
            case 'R':
                return ChessPiece.ROOK;
            case 'B':
                return ChessPiece.BISHOP;
            default:
                return null;
        }
    }

    private static String readFile(String filename) {
        StringBuilder content = new StringBuilder();
        try(Scanner scanner = new Scanner(SolutionLoader.class.getResourceAsStream("/" + filename))) {
            while (scanner.hasNextLine()) {
                content.append(scanner.nextLine()).append("\n");
            }
        }
        return content.toString();
    }
}
